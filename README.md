# Project Eggbert
A tower defense game made for the Autumn game jam

# Dependencies
- SFML 2 

Install on macos: `brew install sfml`\
Install on ubuntu: `sudo apt install libsfml-dev`\
Install on arch: `sudo pacman -S sfml`