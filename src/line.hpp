#ifndef LINE_H
#define LINE_H

#include <SFML/Graphics.hpp>

class Line {
    double x1;
    double y1;
    double x2;
    double y2;
    sf::Vertex line[2];

    public:
        Line(double x1, double y1, double x2, double y2);
        void draw(sf::RenderWindow &win);
};

#endif