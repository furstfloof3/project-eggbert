#ifndef TOWER_HPP
#define TOWER_HPP

#include "SFML/Graphics.hpp"

class Tower {
    int range;
    int DAMAGE;
    float y;
    float x;
    bool isAttacking;
    sf::Vector2f attackPos;

    public:
        Tower(int range, int damage, float y, float x);
        double calculateDistance(sf::Vector2f position);
        int getDamage();
        bool canHitTarget(sf::Vector2f position);
        void draw();
        void update();
};

#endif