#include "tower.hpp"
#include "SFML/Graphics.hpp"
#include <math.h>

Tower::Tower(int range, int damage, float y, float x){
    Tower::range = range;
    Tower::DAMAGE = damage;
    Tower::y = y;
    Tower::x = x;
}

bool Tower::canHitTarget(sf::Vector2f position){
    double distance = Tower::calculateDistance(position);

    if(distance <= Tower::range){
        return true;
    } else {
        return false;
    }
}

// Calculates distance on a 2D plane. Might be less accurate than other methods
// Also, we are only searching for nearby. Maybe we can have a way to change priorities
// This requires more overheard since we would be using a sorting algorithm to sort distances. A fast one, but still more overhead
double Tower::calculateDistance(sf::Vector2f position){
    float x2 = position.x;
    float y2 = position.y;

    double distance = sqrt(pow(x2-Tower::x,2.0) + pow(y2-Tower::y,2.0));

    return distance;
};