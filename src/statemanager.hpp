#ifndef STATEMANAGER_HPP
#define STATEMANAGER_HPP

#include <string>
#include <vector>

struct State {
    std::string name;
    bool state;
};
 
class state{
    std::vector<State> currstate;

    public:
        state();
        void changeState(std::string, bool state);
        State getState(std::string name);
        int getStateIndex(std::string name);
        void addState(std::string, bool state);
};

#endif