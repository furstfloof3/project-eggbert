#ifndef GRID_HPP
#define GRID_HPP

#include <SFML/Graphics.hpp>

class Grid {
    int HEIGHT;
    int LENGTH;
    public:
        Grid(int height, int length);
        void draw(sf::RenderWindow &win);
};

#endif