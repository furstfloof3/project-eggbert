#include <SFML/Graphics.hpp>
#include "grid.hpp"
#include <iostream>
#include <stdlib.h>
#include <vector>
#include "entity.hpp"

int SCREEN_WIDTH = 1280;
int SCREEN_HEIGHT = 720;
int START_X = -64;
int START_Y = SCREEN_HEIGHT/2 - 32;
int SIZE = 16.f;
double VEL = 0.09;

char gameTitle[] = "Project Eggbert";

using namespace sf;
Clock currtime;
std::vector<Entity> shapes;

int main(int argc, char *argv[])
{
    // Forcing windowed mode due to scaling issues :(
    RenderWindow window(VideoMode(SCREEN_WIDTH,SCREEN_HEIGHT), gameTitle, Style::None, ContextSettings(0, 0, 8));

    //shape.setPosition(START_X,START_Y);
    for(int i = 0; i < 1; i++){
        Entity entity(10,-64, SCREEN_HEIGHT/2 - 32, 0.09,16.f);
        entity.setPosition();

        shapes.push_back(entity);
    }

    while (window.isOpen())
    {
        for(int i = 0; i < shapes.size(); i++){
            if(shapes[i].getPosition().x > SCREEN_WIDTH+64){
                shapes.erase(shapes.begin());
                shapes.shrink_to_fit();
            }
        }
        if(currtime.getElapsedTime().asSeconds() >= 2){
            Entity entity(10,-64, SCREEN_HEIGHT/2 - 32, 0.09,16.f);
            entity.setPosition();

            shapes.push_back(entity);

            currtime.restart();
        }
        Event event;
        while (window.pollEvent(event))
        {
            if (event.type == Event::Closed)
                window.close();
        }


        window.clear();
        //grid.draw(window);
        
        for(int i = 0; i <shapes.size(); i++){
            shapes[i].update();
            shapes[i].draw(window);
        }

        //window.setFramerateLimit(75);
        window.display();
    }

    return 0;
}