#include "statemanager.hpp"
#include <string>
#include <vector>

state::state(){
    std::vector<State> currstate;
};

void state::changeState(std::string name, bool state){
    for(int i = 0; i < 10; i++){
        if(state::currstate[i].name == name){
            state::currstate[i].state = state;
        }
    }
};

void state::addState(std::string name, bool state){
    State tmp = {name,state};
    state::currstate.push_back(tmp);
}

int state::getStateIndex(std::string name){
    for(int i = 0; i < 10; i++){
        if(state::currstate[i].name == name){
            return i;
        }
    }

    return -1;
}

State state::getState(std::string name){
    int ind = state::getStateIndex(name);

    return state::currstate[ind];
};