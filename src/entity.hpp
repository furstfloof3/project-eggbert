#ifndef ENTITY_HPP
#define ENTITY_HPP

#include <SFML/Graphics.hpp>

class Entity {
    int health;
    int START_X;
    int START_Y;
    double VEL;
    int SIZE;
    sf::CircleShape shape;

    public:
        Entity(int health, int START_X, int START_Y, double VEL, int SIZE);
        void draw(sf::RenderWindow &win);
        void update();
        void setPosition();
        sf::Vector2f getPosition();
        void onHit(int hitbydamage);
};

#endif