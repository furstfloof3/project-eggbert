#include "line.hpp"
#include "SFML/Graphics.hpp"

Line::Line(double x1, double y1, double x2, double y2)
{
    Line::x1 = x1;
    Line::y1 = y1;
    Line::x2 = x2;
    Line::y2 = y2;
    Line::line[2] = sf::Vertex{}; // I hate vertexes
    Line::line[0] = sf::Vector2f(Line::x1, Line::y1);
    Line::line[1] = sf::Vector2f(Line::x2, Line::y2);
};

void Line::draw(sf::RenderWindow &win){
    win.draw(Line::line, 2, sf::Lines);
};