#include "entity.hpp"
#include "SFML/Graphics.hpp"
#include <iostream>


Entity::Entity(int health, int START_X, int START_Y, double VEL, int SIZE){
    Entity::health = health;
    Entity::START_X = START_X;
    Entity::START_Y = START_Y;
    Entity::VEL = VEL;
    Entity::SIZE = SIZE;
    Entity::shape = sf::CircleShape(16.f);
};

void Entity::draw(sf::RenderWindow &win){
    win.draw(shape);
}

void Entity::update(){
    shape.move(Entity::VEL,0.f);
}

void Entity::setPosition(){
    shape.setPosition(Entity::START_X, Entity::START_Y);
}

sf::Vector2f Entity::getPosition(){
    return shape.getPosition();
}

void Entity::onHit(int hitbydamage){
    Entity::health -= hitbydamage;
}