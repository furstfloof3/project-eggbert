#include <SFML/Graphics.hpp>
#include "grid.hpp"
using namespace sf;

Grid::Grid(int height, int length){
    HEIGHT = height;
    LENGTH = length;
};

void Grid::draw(RenderWindow &win){
    // I copied this code, shamelessly (with my own modifications)
    int lines = HEIGHT+LENGTH-2;

    VertexArray grid(sf::Lines, 2*(lines));

    win.setView(win.getDefaultView());

    auto size = win.getView().getSize();

    float rowH = size.y/LENGTH;
    float colW = size.x/HEIGHT;

    // row seperators
    for(int i = 0; i < LENGTH-1;i++){
        int r = i+1;

        float rowY = rowH*r;

        grid[i*2].position.x = 0;
        grid[i*2].position.y = rowY;

        grid[i*2+1].position.x = size.x;
        grid[i*2+1].position.y = rowY;
    }

    // Column seperators
    for(int i = LENGTH-1; i < lines; i++){
        int c = i-LENGTH+2;
        float colX = colW*c;

        grid[i*2].position.x = colX;
        grid[i*2].position.y = 0;

        grid[i*2+1].position.x = colX;
        grid[i*2+1].position.y = size.y;
    }

    win.draw(grid);
};